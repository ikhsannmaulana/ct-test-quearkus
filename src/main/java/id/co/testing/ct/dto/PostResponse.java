package id.co.testing.ct.dto;

import id.co.testing.ct.entity.Tag;
import lombok.Data;

import java.util.List;

@Data
public class PostResponse {
    public String id;
    public String title;
    public String content;
    public List<Tag> tags;
}
