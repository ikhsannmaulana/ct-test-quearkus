package id.co.testing.ct.entity;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "POST_TAG")
public class PostTag {

    @Id
    private String id;

    @Column(name = "POST_ID")
    String postId;

    @Column(name = "TAG_ID")
    String tagId;
}
