package id.co.testing.ct.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "TAG")
public class Tag {


    @Id
    private String id;

    @Column(name = "LABEL")
    private String label;

}
