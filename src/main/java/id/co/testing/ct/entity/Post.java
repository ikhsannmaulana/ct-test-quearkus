package id.co.testing.ct.entity;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "POST")
public class Post {

    @Id
    private String id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CONTENT")
    private String content;

}
