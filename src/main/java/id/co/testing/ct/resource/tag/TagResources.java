package id.co.testing.ct.resource.tag;

import id.co.testing.ct.dto.*;
import id.co.testing.ct.entity.Tag;
import id.co.testing.ct.repositories.TagRepository;
import id.co.testing.ct.service.TagService;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;
import java.util.Optional;

@Path("/tag")
public class TagResources extends TagService {

    @Inject
    TagRepository tagRepository;

    @GET
    @Produces("application/json")
    public List<TagResponse> getAllTag() {
        List<Tag> tags=tagRepository.findAll();
        return setResponses(tags);
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public TagResponse findById(@PathParam String id) {
        Optional<Tag> tagOptional=tagRepository.findById(id);
        if(tagOptional.isPresent()){
            Tag tag=tagOptional.get();
            return setResponse(tag) ;
        }
        throw new IllegalArgumentException("No Tag with id " + id + " exists");

    }

    @PUT
    @Path("/{id}")
    @Produces("application/json")
    public TagResponse update2(@PathParam String id, TagRequest tagRequest) {
        Optional<Tag> tagOptional=tagRepository.findById(id);
        if(tagOptional.isPresent()){
            Tag tag=tagOptional.get();
            tag.setLabel(tagRequest.getLabel());
            tagRepository.save(tag);
            return setResponse(tag) ;
        }
        throw new IllegalArgumentException("No Tag with id " + id + " exists");

    }

    @POST
    @Produces("application/json")
    public TagResponse addPost(TagRequest tagRequest) {
        return addNewPost(tagRequest) ;
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam String id) {
        deletePostTagByPostId(id);
        tagRepository.deleteById(id);
    }
}
