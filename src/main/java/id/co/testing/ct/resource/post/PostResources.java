package id.co.testing.ct.resource.post;

import id.co.testing.ct.dto.PostAddRequest;
import id.co.testing.ct.dto.PostResponse;
import id.co.testing.ct.entity.Post;
import id.co.testing.ct.repositories.PostRepository;
import id.co.testing.ct.service.PostService;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;
import java.util.Optional;

@Path("/post")
public class PostResources extends PostService {

    @Inject
    PostRepository postRepository;


    @GET
    @Produces("application/json")
    public List<PostResponse> getALlPost() {
        List<Post> posts = postRepository.findAll();
        return setResponses(posts);


    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public PostResponse findById(@PathParam String id) {
        Optional<Post> post=postRepository.findById(id);
        if(post.isPresent()){
            Post post1=post.get();
            return setResponse(post1) ;
        }
        throw new IllegalArgumentException("No Post with id " + id + " exists");

    }

    @PUT
    @Path("/{id}")
    @Produces("application/json")
    public PostResponse update2(@PathParam String id, PostAddRequest postAddRequest) {
        Optional<Post> post=postRepository.findById(id);
        if(post.isPresent()){
            Post post1=post.get();
            return updatePostResponse(post1,postAddRequest) ;
        }
        throw new IllegalArgumentException("No Post with id " + id + " exists");

    }

    @POST
    @Produces("application/json")
    public PostResponse addPost(PostAddRequest postAddRequest) {
        return addNewPost(postAddRequest) ;
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam String id) {
        deletePostTagByPostId(id);
        postRepository.deleteById(id);
    }
}
