package id.co.testing.ct.repositories;

import id.co.testing.ct.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag,String>  {


}
