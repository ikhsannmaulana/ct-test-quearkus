package id.co.testing.ct.repositories;

import id.co.testing.ct.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post,String>  {
}
