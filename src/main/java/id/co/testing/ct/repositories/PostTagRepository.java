package id.co.testing.ct.repositories;

import id.co.testing.ct.entity.PostTag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostTagRepository extends JpaRepository<PostTag,String>  {

    List<PostTag> findByPostId(String postId);
    void deleteByPostId(String postId);

    List<PostTag> findByTagId(String postId);
    void deleteByTagId(String postId);


}
