package id.co.testing.ct.service;


import id.co.testing.ct.dto.PostAddRequest;
import id.co.testing.ct.dto.PostResponse;
import id.co.testing.ct.entity.Post;
import id.co.testing.ct.entity.PostTag;
import id.co.testing.ct.entity.Tag;
import id.co.testing.ct.repositories.PostRepository;
import id.co.testing.ct.repositories.PostTagRepository;
import id.co.testing.ct.repositories.TagRepository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class PostService {

    @Inject
    PostTagRepository postTagRepository;
    @Inject
    TagRepository tagRepository;
    @Inject
    PostRepository postRepository;


    protected List<PostResponse> setResponses(List<Post> posts){
        List<PostResponse> postResponses= new ArrayList<>();
        posts.forEach(
                post -> postResponses.add(setResponse(post))
        );

        return postResponses;
    }

    protected PostResponse setResponse(Post post){
        List<Tag> tags= getListTags(post.getId());
        PostResponse postResponse=new PostResponse();
        postResponse.setId(post.getId());
        postResponse.setTitle(post.getTitle());
        postResponse.setContent(post.getContent());
        postResponse.setTags(tags);
        return postResponse;
    }

    private List<Tag> getListTags(String id){
        List<PostTag> listIdTag=postTagRepository.findByPostId(id);
        List<Tag> tags= new ArrayList<>();
        listIdTag.forEach(
                tag->tags.add(getIdTag(tag))
        );
        return tags;
    }

    private Tag getIdTag(PostTag postTag){
        return tagRepository.findById(postTag.getTagId()).get();
    }

    protected PostResponse updatePostResponse(Post post, PostAddRequest postRequest){
        deletePostTagByPostId(post.getId());

        post.setContent(postRequest.getContent());
        post.setTitle(postRequest.getTitle());
        postRepository.save(post);

        List<Tag> tags=saveAndGetTags(post.getId(),postRequest.getIdTag());
        PostResponse postResponse=new PostResponse();
        postResponse.setId(post.getId());
        postResponse.setTitle(post.getTitle());
        postResponse.setContent(post.getContent());
        postResponse.setTags(tags);

        return postResponse;
    }

    protected void deletePostTagByPostId(String postId){
        postTagRepository.deleteByPostId(postId);
    }

    private List<Tag> saveAndGetTags (String postId,List<String> postRequest){
        List<Tag> tags=new ArrayList<>();
        postRequest.forEach(
                id->tags.add(addNewTag(postId,id))
        );
        return tags;
    }

    private Tag addNewTag(String postId,String id){
        Tag tag= new Tag();
        Optional<Tag> tagOptional=tagRepository.findById(id);
        if(tagOptional.isPresent()) {
            tag=tagOptional.get();
            PostTag postTag= new PostTag();
            postTag.setId(UUID.randomUUID().toString());
            postTag.setPostId(postId);
            postTag.setTagId(id);
            postTagRepository.save(postTag);
        }
        return tag;
    }

    protected PostResponse addNewPost(PostAddRequest postAddRequest){
        PostResponse postResponse=new PostResponse();
        Post post=new Post();
        post.setId(UUID.randomUUID().toString());
        post.setTitle(postAddRequest.getTitle());
        post.setContent(postAddRequest.getContent());
        postRepository.save(post);
        List<Tag> tags= saveAndGetTags(post.getId(),postAddRequest.getIdTag());

        postResponse.setId(post.getId());
        postResponse.setTitle(post.getTitle());
        postResponse.setContent(post.getContent());
        postResponse.setTags(tags);

        return postResponse;


    }
}
